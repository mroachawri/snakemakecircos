# SnakemakeCircos

## Dependencies

 - Snakemake (tested with 4.3.1)
 - Circos (tested with 0.69-6)
 - VarScan (tested with 2.3.9)
 - Samtools
 - Bedtools
 - numpy (python module)
 
## Setup

 - Modify `SnakemakeCircos.py` to point to your installation of circos and to your VarScan .jar file
  
## Usage

 - Map reads to your genome
 - Run the pipeline (specify CPUs with`-j`):

`snakemake  -s SnakemakeCircos.py -j 4  --config genome=filename  bam=filename`

## Test

```
#!text
cd test/
snakemake  -s ../SnakemakeCircos.py -j 4  --config genome=contigs.fa  bam=aligned.bam
```