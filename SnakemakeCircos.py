#!/usr/bin/env snakemake

# Copyright (c) 2019 Michael Roach (Australian Wine Research Institute)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import re
import os
import numpy as np


# usage 
# snakemake  -s SnakemakeCircos.py  -j 4  --config ref=filename  bam=filename


### CONFIGURE ###
varscan = "/home/mike/VarScan.v2.3.9.jar"
circos = "/home/mike/circos-0.69-6/bin/circos"

### PARAMS ###

# command line arguments
genome = config['ref']
bam = config['bam']

# other
genName = re.sub('.fa[sta]+$', '', genome)


### TARGETS ###
snpHist = genName + ".snp"
covHist = genName + ".cov"
karFile = genName + ".kar"
circConf = genName + ".circos.conf"
circPng = genName + ".circos.png"


### PIPELINE ###

rule makeCircosPlot:
    input:
        circPng


rule callSNPs:
    input:
        gen=genome,
        bam=bam, 
        req=bam + ".bai",
        fai=genome + ".fai"
    output:
        genName + ".snp.vcf"
    shell:
        "samtools mpileup -A -x -B -f {input.gen} {input.bam} \
            | java -jar {varscan} mpileup2snp --p-value 1e-5 --output-vcf \
            | grep -P '#|HET=1' \
            > {output}"


rule fastaIndex:
    input:
        "{fastaFile}"
    output:
        "{fastaFile}.fai"
    shell:
        "samtools faidx {input}"


rule bamIndex:
    input:
        "{bamFile}.bam"
    output:
        "{bamFile}.bam.bai"
    shell:
        "samtools index {input}"


rule bedtoolsMakewindows:
    input:
        genome + ".fai"
    output:
        genName + ".windows.bed"
    run:
        fai = open(input[0], 'r')
        genLen = 0
        for line in fai:
            l = re.split('\s+', line)
            genLen += int(l[1])
        fai.close()
        windowSize = np.ceil(genLen / 1000000) * 1000
        stepSize = int(windowSize / 2)
        shell("bedtools makewindows -g {input} -w {windowSize} -s {stepSize} > {output}")


rule bedtoolsMulticov:
    input:
        bam=bam,
        bed=genName + ".windows.bed",
        req=bam + ".bai"
    output:
        covHist
    shell:
        "bedtools multicov -bams {input.bam} -bed {input.bed} \
        | awk '{{print $1\"\t\"$2\"\t\"$3\"\t\"$4}}' \
        > {output}"


rule vcf2bed:
    input:
        genName + ".snp.vcf"
    output:
        genName + ".snp.bed"
    shell:
        "grep -v \# {input} | awk '{{print $1\"\t\"$2\"\t\"$2\"\t\"1}}' > {output}"


rule bedtoolsMap:
    input:
        bed=genName + ".windows.bed",
        snp=genName + ".snp.bed"
    output:
        snpHist
    shell:
        "bedtools map -a {input.bed} -b {input.snp} -c 4 \
            | awk '{{print $1\"\t\"$2\"\t\"$3\"\t-\"$4}}' \
            | sed 's/-\./0/' \
            > {output}"


rule runCircos:
    input:
        conf = circConf,
        kar = karFile,
        cov = covHist,
        snp = snpHist
    output:
        circPng
    shell:
        "{circos} -conf {input.conf} -outputfile {output}"




rule generateKarFile:
    input:
        genome + ".fai"
    output:
        karFile
    run:
        genSize = 0
        fai = open(input[0], 'r')
        ctgs = {}
        for line in fai:
            l = re.split('\s+', line)
            genSize += int(l[1])
            ctgs[l[0]]=int(l[1])
        fai.close()
        col = "white"
        out = open(output[0], 'w')
        for contig, ctgLen in sorted(ctgs.items(), key=lambda item: item[1], reverse = True):
            out.write("chr - %s %s 0 %d %s\n" % (contig, contig, ctgLen, col))
            if (col == 'white'):
                col = 'black'
            else:
                col = 'white'


rule generateCircosConfig:
    input:
        covHist,
        snpHist,
        genome + ".fai"
    output:
        circConf
    run:
        # slurp the snp and cov densities
        covFile = open(input[0], 'r')
        covDens = []
        for line in covFile:
            l = re.split('\s+', line)
            covDens.append(int(l[3]))
        covFile.close()
        snpFile = open(input[1], 'r')
        snpDens = []
        for line in snpFile:
            l = re.split('\s+', line)
            snpDens.append(int(l[3]))
        snpFile.close()
        
        # calc the percentiles
        covPc = {}
        snpPc = {}
        covPc['75'] = np.percentile(covDens, 75)
        covPc['95'] = np.percentile(covDens, 95)
        snpPc['20'] = np.percentile(snpDens, 20)
        snpPc['1'] = np.percentile(snpDens, 1)
        
        # calc the chrom units as 100th the genome size
        fai = open(input[2], 'r')
        chromUnit = 0
        for line in fai:
            l = re.split('\s+', line)
            chromUnit += int(l[1])
        fai.close()
        chromUnit = np.ceil(chromUnit / 100000) * 1000
        
        # write the output
        out = open(output[0], 'w')
        out.write("""
karyotype = %s

chromosomes_units = %d

<<include colors_fonts_patterns.conf>>
<<include housekeeping.conf>> 

# IMAGE
<image>
<<include image.conf>>
</image>

# IDEOGRAM
<ideogram>
<spacing>
default = 0.001r
</spacing>
radius           = 0.8r
thickness        = 40p
fill             = yes
stroke_color     = black
stroke_thickness = 2p
show_label       = yes
label_radius     = dims(image,radius) - 200p
label_size       = 30p
label_parallel   = no
</ideogram>

# TICKS
show_ticks          = yes
show_tick_labels    = no
<ticks>
radius           = 1r
color            = black
thickness        = 2p
<tick>
spacing        = 1u
size           = 20p
</tick>
</ticks>

# PLOTS
<plots>
type       = histogram
extend_bin = no
color      = black
fill_under = yes
thickness  = 1
<plot>
file = %s
r0   = 0.75r
r1   = 0.975r
min  = 0
max  = %d
<rules>
<rule>
condition = var(value) > %d
fill_color = vdred
</rule>
<rule>
condition = 1
fill_color = eval(sprintf("rdbu-7-div-%%d",remap_int(var(value),0,%d,1,7)))
</rule>
</rules>
</plot>

<plot>
file = %s
r0   = 0.5r
r1   = 0.725r
min  = %d
max  = 0
<rules>
<rule>
condition = 1
fill_color = eval(sprintf("rdbu-7-div-%%d",remap_int(var(value),%d,0,7,1)))
</rule>
</rules>
</plot>

# BACKGROUND
background                  = yes
background_stroke_color     = black
background_stroke_thickness = 2
</plots>
"""     % (karFile, chromUnit, covHist, covPc['95'], covPc['95'], covPc['75'], snpHist, snpPc['1'],  snpPc['20']))
        


